��    6      �  I   |      �     �     �     �     �  
   �  
   �     �     �  
   �        
             #  	   2     <     E     I     h  9   z     �     �     �  	   �     �  "        '  8   E  %   ~  
   �  "   �     �     �       
        %     .     <     H     N     U     h     u  	   ~     �     �     �     �     �     �     �     �     �     �  �  �     i	     x	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     "
     8
  F   V
     �
     �
  !   �
     �
     �
  !   �
     !  @   @  0   �  
   �  #   �     �     �       
   /  
   :     E     S  	   Y  	   c     m     �     �     �     �  
   �     �     �     �     �     �  	   �     �          .             +   2   0   "       -                 3              (   4                  	   *   %          #   ,       1   6      $      )                                         &                                  !            /   5   '       
               Beer philosopher Close Contact Content Content EN Content SK Email address Episodes Host Hosts Icon coffee Icon heart Icon people Internal title Listen on Logo SLH Map Map to the place of occurrence No speakers found Page '{0}' can not be deleted. You can only unpublish it. Photo of Ladislav Hanus Photo of SLH fellowship Privacy policy Programme Submit Supports Youtube, Vimeo and {}{}{} The Ladislav Hanus Fellowship The page title as you'd like it to be seen by the public URL to Google Maps or similar service URL to map Visible in header next to the logo Visible only in the admin We are working on this... We use 🍪 cookies categories category date and time description event events festival beginning festival end location locations more more about SLH next other websites place previous speaker speakers title year Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
 Pivný filozof Zavrieť Kontakt Obsah Obsah EN Obsah SK Emailová adresa Epizódy Moderátor Moderátori Ikona káva Ikona srdce Ikona ľudia Interný názov Počúvaj na Logo SLH Mapa Mapa k miestu konania Nenašli sa žiadni rečníci Stránka '{0}' nemôže byť vymazaná. Môžete ju iba odpublikovať. Fotka Ladislava Hanusa Fotka spoločenstva SLH Zásady ochrany osobných údajov Program Odoslať Podporuje Youtube, Vimeo a {}{}{} Spoločenstvo Ladislava Hanusa Názov stránky, ako by ste chceli, aby sa zobrazoval verejnosti URL adresa na Google Mapy alebo obdobnú službu URL k mape Viditeľné v hlavičke vedľa loga Viditeľné iba v adminovi Na programe sa pracuje... Používame 🍪 cookies kategórie kategória dátum a čas popis podujatie podujatia začiatok festivalu koniec festivalu poloha polohy ďalší viac o SLH ďalší ďalšie stránky miesto predchádzajúci rečník rečníci titulok rok 